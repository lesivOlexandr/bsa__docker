FROM node:12-alpine

COPY client ./client
COPY server ./server
COPY start.sh ./

WORKDIR /client
RUN npm install
RUN npm run build

WORKDIR /server
RUN npm install

CMD ["npm", "start"]
EXPOSE $PORT