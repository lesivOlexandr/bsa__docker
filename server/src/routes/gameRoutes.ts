import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { texts } from '../data';

const router: Router = Router();

router
  .get("/", (_, res) => {
    const page: string = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })
  .get('/texts/:id', (req, res) => {
    const text: string = texts[+req.params.id];
    res.send(text);
  })
  ;

export default router;