export const getRandomNumber = (upperLimit: number): number => {
  const randInt = Math.floor(Math.random() * upperLimit);
  return randInt;
}

export const calcProgress = (text: string, input: string): number => {
  if (!text?.length || !input?.length) {
    return 0;
  }

  let count: number = 0;
  for (let char of input) {
    if (char === text[count]) {
      count += 1;
    }
  }
  return count * 100 / text.length;
} 