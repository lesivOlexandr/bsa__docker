export enum actions {
  // incoming
  createRoom = 'CREATE_ROOM',
  joinRoom = 'JOIN_ROOM',
  leaveRoom = 'LEAVE_ROOM',
  toggleReady = 'TOGGLE_READY',

  // outgoing
  updateRooms = 'UPDATE_ROOMS',
  userNameIsNotUnique = 'ERROR_USER_NAME_IS_NOT_UNIQUE',
  roomNameIsNotUnique = 'ERROR_ROOM_NAME_IS_NOT_UNIQUE',
  error = 'ERROR',
  userJoined = 'USER_JOINED',
  roomUpdate = 'ACTIVE_ROOM_UPDATE',
  userStateUpdate = 'USER_STATE_UPDATE',
  usersReady = 'ALL_ROOM_USERS_READY',
  challengeStart = 'START_CHALLENGE',
  challengeEnd = 'END_CHALLENGE',
  botMessage = 'BOT_MESSAGE'
}