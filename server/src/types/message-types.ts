export enum messageTypes {
  introduction, 
  start,
  regular, 
  nearingFinish, 
  finish, 
  complete
}