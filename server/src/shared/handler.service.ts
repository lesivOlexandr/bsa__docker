import { ActionsService } from './actions.service';
import { DataStorage, dataStorage } from '../shared/data-storage';
import { getRandomNumber, calcProgress } from '../helpers/socketHelpers';
import { texts } from '../data';
import { Socket } from 'socket.io';
import { actions } from '../types/actions';
import { Room, roomStates, GameResult, User, Progress } from '../types';
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from '../socket/config';
import { CommentatorService } from './commentator.service';

export class HandlerService {
  private actionService: ActionsService;
  private storage: DataStorage = dataStorage;
  private commentator: CommentatorService; 

  constructor(socket: Socket){
    this.actionService = new ActionsService(socket);
    this.commentator = new CommentatorService(socket);
  }
  public createRoom(roomName: string, userName: string): void {
    if (!roomName || !userName) {
      this.actionService.emitToUser(actions.error, 'You should provide a valid room name and username');
      return;
    }
    if (this.storage.getRoomByName(roomName)) {
      this.actionService.emitToUser(actions.roomNameIsNotUnique, 'Room name is not unique');
      return;
    }
    this.storage.addRoom(new Room(roomName));
    this.joinRoom(roomName, userName);
  };

  public joinRoom(roomName: string, username: string): void {
    const room: Room | undefined = this.storage.getRoomByName(roomName);
    if (!room) {
      this.actionService.emitToUser(actions.error, 'Failed to join');
      return;
    }
    if (this.storage.userHasRoom(username)) {
      this.storage.removeUserFromRoom(username);
    }
    const user = new User(username);
    this.storage.addUserToRoom(user, roomName);
    this.actionService.joinToRoom(room.name);
    this.commentator.onIntroduce(user);
    this.actionService.emitToUser(actions.userJoined, room);
    this.actionService.emitToRoom(actions.roomUpdate, room, room);
    this.updateAllRooms();
  }

  public updateAllRooms(): Room[] {
    const rooms: Room[] = this.storage.getRooms();
    const joinableRooms: Room[] = rooms.filter(room => room.state === roomStates.pending && !room.maxUsersReached);
    const dataToSend = joinableRooms.map(room => ({
      name: room.name, 
      usersCount: room.users.length || 0
    }));
    this.actionService.emitToAll(actions.updateRooms, dataToSend);
    return rooms;
  }

  public getGameResult(room: Room): GameResult[] {
    const result: GameResult[] = room.users.map((user: User) => {
      return ({ 
        username: user.name, 
        spentSeconds: user?.progress?.spentSeconds 
      });
    });
    return result.sort((result1, result2) => (result1.spentSeconds || Infinity) - (result2.spentSeconds || Infinity));
  }

  public checkAllRoomUsersReady(room: Room): boolean {
    if (room.state === roomStates.pending && room.users.every(user => user.progress.isReady)) {
      room.state = roomStates.timerStarted;
      const textIdx = getRandomNumber(texts.length);
      
      this.actionService.emitToRoom(actions.usersReady, room, {
          seconds: SECONDS_TIMER_BEFORE_START_GAME,
          textIdx
      });
      room.text = texts[textIdx];
      this.updateAllRooms();

      setTimeout(() => {
        room.state = roomStates.gameStarted;
        room.timeStarted = Date.now();
        this.actionService.emitToRoom(actions.challengeStart, room, SECONDS_FOR_GAME);
        this.commentator.onStart(room);

        const timeOutId: NodeJS.Timeout = setTimeout(() => {
          if (room.state === roomStates.gameStarted) {
            const gameResult: GameResult[] = this.getGameResult(room);
            
            this.actionService.emitToRoom(actions.challengeEnd, room, gameResult);
            this.storage.dropRoomState(room.name);
            this.actionService.emitToRoom(actions.roomUpdate, room, room);
            this.commentator.onGameEnd(gameResult, room);
            this.updateAllRooms();
          };
        }, SECONDS_FOR_GAME * 1000);
        this.storage.setRoomTimerPair(room, timeOutId);
      }, SECONDS_TIMER_BEFORE_START_GAME * 1000);

      return true;
    }
    return false;
  }

  public checkAllUsersDone(room: Room): boolean {
    if (room.state === roomStates.gameStarted && room.users.every(user => user.progress.doneInPercents === 100)) {
      this.storage.clearRoomTimer(room);
      const result: GameResult[] = this.getGameResult(room);
      this.actionService.emitToRoom(actions.challengeEnd, room, result);
      this.storage.dropRoomState(room.name);
      this.actionService.emitToRoom(actions.roomUpdate, room, room);
      this.updateAllRooms();
      this.commentator.onGameEnd(result, room);
      return true;
    }
    return false;
  }

  public handleLeaveRoom(username: string): void {
    const userRoom: Room | undefined = this.storage.getUserRoom(username);
    if (!userRoom) {
      this.actionService.emitToUser(actions.error,'Failed to leave room');
      return;
    }
    this.storage.removeUserFromRoom(username);
    this.actionService.leaveRoom(userRoom.name);
    if (this.storage.checkRoomActiveness(userRoom)) {
      this.actionService.emitToRoom(actions.roomUpdate, userRoom, userRoom);
      this.checkAllRoomUsersReady(userRoom);
      this.checkAllUsersDone(userRoom);
    }
    this.updateAllRooms();
  }

  public handleUserStateUpdate(username: string, progress: Progress): void {
    const userRoom: Room | undefined = this.storage.getUserRoom(username);
    if (userRoom) {
      const currUser: User = userRoom.users.find(user => user.name === username)!;
      progress.doneInPercents = calcProgress(userRoom.text!, progress.inputtedText);
      Object.assign(currUser, { progress });
      this.actionService.emitToRoom(actions.userStateUpdate, userRoom, currUser);
      if (progress.doneInPercents === 100) {
        this.commentator.onFinish(currUser, userRoom);
      }
      if (
        userRoom.text &&
        userRoom.text!.length > 30 &&
        currUser.progress.inputtedText.length + 30 === userRoom.text!.length)
      {
        this.commentator.onNearFinish(currUser, userRoom);
      }
      this.checkAllUsersDone(userRoom);
    }
  }

  public handleToggleReady(username: string, progress: Progress): void {
    const userRoom: Room | undefined = this.storage.getUserRoom(username);
    if (userRoom) {
      this.handleUserStateUpdate(username, progress);
      this.checkAllRoomUsersReady(userRoom);
    }
  }

  public handleUserDisconnect(username: string): void {
    if (this.storage.disconnectUser(username)){
      this.handleLeaveRoom(username);
    }
  }

  public handleUserConnect(username: string): boolean {
    if (this.storage.connectUser(username)) {
      this.updateAllRooms();
      return true;
    }
    return false;
  }
}