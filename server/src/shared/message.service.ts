import { getRandomNumber } from "../helpers/socketHelpers";
import { messageTypes } from "../types/message-types";
import { User, Room, GameResult, MessageFormatterFunc } from "../types";
import { SECONDS_FOR_GAME } from "../socket/config";

export class MessageFactory {
  private introductionMessages: IntroductionMessages = new IntroductionMessages;
  private startMessages: StartMessages = new StartMessages;
  private regularMessages: RegularMessages = new RegularMessages;
  private nearFinishMessages: NearingFinishMessages = new NearingFinishMessages;
  private finishMessages: FinishMessages = new FinishMessages;
  private completeMessages: CompleteMessages = new CompleteMessages;

  get(type: messageTypes, data: any) {
    if (type === messageTypes.introduction){
      return this.introductionMessages.get(data);
    }
    if (type === messageTypes.start){
      return this.startMessages.get(data);
    } 
    if (type === messageTypes.regular){
      return this.regularMessages.get(data);
    }
    if (type === messageTypes.nearingFinish){
      return this.nearFinishMessages.get(data);
    }
    if (type === messageTypes.finish){
      return this.finishMessages.get(data);
    }
    if (type === messageTypes.complete){
      return this.completeMessages.get(data);
    }
    return '';
  }
}

abstract class Messages {
  constructor(protected messages: MessageFormatterFunc[] = []){}

  public get(room: Room): string {
    const length = this.messages.length;
    if (!length) {
      throw new Error('There is no messages');
    }
    const randInt = getRandomNumber(length);
    return this.messages[randInt](room);
  }

  public add(formatter: MessageFormatterFunc) {
    this.messages.push(formatter);
    return formatter;
  }
}

class IntroductionMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1);
  }

  message1(): string {
    return (`
      Хей, хей, хей вітаю з вами я - клавіфікований коментатор справжніх кіберспотривних матчів
      справжнього кіберспорту. Мене звати - Постфікс Андронович, і ми готові починати, залишилося лише зачекати учасників.
    `);
  }
}

class StartMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1);
  };

  message1(room: Room): string {
    const users = room.users;
    let possibleVariants: string[] = [
      ' на своєму зеленому Maserati',
      ', що керує своїм помаранчевий Lamborgini',
      ', який ефектно прибув на загадковій Infinity',
      ' - учасник, що буде брати участь у заїзді на швидкісному потязі',
      ', прибувши до порту на червоній volvo'
    ];
    
    let getTransportMessage = () => {
      if (possibleVariants.length) {
        const length: number = possibleVariants.length;
        const upperLimit: number = possibleVariants.length
        return possibleVariants.splice(getRandomNumber(upperLimit), 1)
      }
      return ' - схоже ніякою інформацією, щодо цього учасника ми не володіємо'
    }

    return `
      Наші учасники підготували свої колісниці і вже готові до заїзду. Саме час їх представити:
        ${users.map((user, i) => (i + 1) + '. ' + user.name + getTransportMessage()).join('\n')}
    `
  }
}

class RegularMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1)
  }

  message1(room: Room): string {
    const now: number = Date.now();
    const secondsPassed: number = Math.round((now - room.timeStarted) / 1000);
    const users: User[] = [...room.users];
    const sortedUsers = users.sort((user1: User, user2: User) => user2.progress.doneInPercents - user1.progress.doneInPercents);

    return (`
      Пройшло ${secondsPassed} секунд з початку заїзду тож покажемо результати:
      ${sortedUsers.map((user, i) => `
        ${user.name} знаходиться на ${i + 1} місці надрукувавши 
        ${user.progress.inputtedText.length} символів ${i !== users.length - 1 ? ';': '.'}
      `).join('\n')}
      До кінця гонки залишилося ${SECONDS_FOR_GAME - secondsPassed} секунд
    `);
  };
}

class NearingFinishMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1);
  }

  message1(user: User): string {
    return (`
      Я чую, як ревуть двигуни учасника ${user.name}, що на повних парах несеться до фінішу. Залишилося зовсім трохи
    `)
  }
}

class FinishMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1);
  }

  message1({room, user}: {room: Room, user: User}): string {
    const users = [...room.users].sort(
      (user1, user2) => (user1.progress.spentSeconds || Infinity) - (user2.progress.spentSeconds || Infinity)
    )
    const position: number = users.findIndex(roomUser => user.name === roomUser.name) + 1;
    if (position === 1) {
      return `І під бурні аплодисменти публіки фінішну пряму першим перетинає гравець ${user.name}!`
    }
    else if (position === room.users.length) {
      return ` Останнім до фінішу приходить ${ user.name }`
    }
    return `На шаленій швидкості фінішну пряму перетинає гравець ${user.name}, що знаходиться на ${position} місці`
  }
}

class CompleteMessages extends Messages {
  constructor(messages: MessageFormatterFunc[] = []){
    super(messages);
    this.add(this.message1);
  }
  
  message1(result: GameResult[]): string {
    const gameResult = result
      .filter(result => result.spentSeconds) // only users that typed all the text will be shown
      .sort((result1, result2) => (result1.spentSeconds || Infinity) - (result2.spentSeconds || Infinity))
      .slice(0, 3);
    return `
      Прийшов час оголосити результати гри:
      ${gameResult.map((result, i) => `
        ${i + 1}. ${result.username} витративши ${result.spentSeconds} секунд ${i === gameResult.length - 1 ? '.': '; '}`)
        .join('')}
    `
  }
}

export const messageFactory = new MessageFactory;