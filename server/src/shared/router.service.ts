import { ActionsService } from "./actions.service";
import { Socket } from "socket.io";
import { actions } from "../types/actions";
import { HandlerService } from "./handler.service";
import { Progress } from "../types";

export class Routing {
  handlerService: HandlerService;
  username: string;
  constructor(socket: Socket){
    this.handlerService = new HandlerService(socket);
    this.username = socket.handshake.query.username;
    this.handlerService.handleUserConnect(this.username);

    socket.on(actions.createRoom, (roomName: string) => this.handlerService.createRoom(roomName, this.username));
    socket.on(actions.joinRoom, (roomName: string) => this.handlerService.joinRoom(roomName, this.username));
    socket.on(actions.leaveRoom, () => this.handlerService.handleLeaveRoom(this.username));
    socket.on(actions.toggleReady, (progress: Progress) => this.handlerService.handleToggleReady(this.username, progress));
    socket.on(actions.userStateUpdate, (progress: Progress) => this.handlerService.handleUserStateUpdate(this.username, progress));
    socket.on('disconnect', () => this.handlerService.handleUserDisconnect(this.username));
  }
}