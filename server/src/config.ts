import path from 'path';

export const STATIC_PATH: string = path.join(__dirname, '../build');
export const JS_FILES_PATH: string = path.join(__dirname, '../build/javascript');
export const HTML_FILES_PATH: string = path.join(__dirname, '../build/html');

export const PORT: string = process.env.PORT || '8080';
