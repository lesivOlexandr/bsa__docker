export enum actions {
  // incoming
  roomNameNotUnique = 'ERROR_ROOM_NAME_IS_NOT_UNIQUE',
  userNameNotUnique = 'ERROR_USER_NAME_IS_NOT_UNIQUE',
  updateRooms = 'UPDATE_ROOMS',
  userJoined = 'USER_JOINED',
  activeRoomUpdate = 'ACTIVE_ROOM_UPDATE',
  userStateUpdate = 'USER_STATE_UPDATE',
  usersReady = 'ALL_ROOM_USERS_READY',
  startChallenge = 'START_CHALLENGE',
  endChallenge = 'END_CHALLENGE',
  botMessage = 'BOT_MESSAGE',

  // outgoing
  createRoom = 'CREATE_ROOM',
  toggleReady = 'TOGGLE_READY',
  joinRoom = 'JOIN_ROOM',
  leaveRoom = 'LEAVE_ROOM',
  
}
