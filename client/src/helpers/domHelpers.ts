interface createElementInterface {
  tagName: string,
  className?: string,
  attributes?: {
    [key: string]: string
  }
}

export const createElement = <T extends HTMLElement>({ tagName, className, attributes = {} }: createElementInterface) => {
  const element: T = <T>document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element: HTMLElement, className: string) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element: HTMLElement, className: string) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = (className: string) => className.split(" ").filter(Boolean);