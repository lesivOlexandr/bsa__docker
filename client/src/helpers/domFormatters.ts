import { User, GameResult } from '../types/index';
import { appState } from '../state';
import { actions } from '../constants/actions';
import { createElement } from './domHelpers';

export const formateRoomUserView = ({ name, progress }: User, currentUserName: string) => {
  return (`
    <div class="users-container__user ${name === currentUserName ? 'user__exact': ''}">
      <div class="user__meta">
        <div class="${progress.isReady ? 'ready': 'nready'} circle">

        </div>
      <span class="user-meta__name">
        ${name} ${name === currentUserName ? '<span>(You)</span>': ''}
      </span>
      </div>
      <div class="user__progress">
        <div 
          class="progress__indicator ${progress.doneInPercents === 100 ? 'progress__indicator-done' : ''}"
          style="width: ${ progress.doneInPercents || 0 }%">
        </div>
      </div>
    </div>
  `);
};

export const formateInputTextInner = (text: string, inputted: string) => {
  let index = 0;
  let innerHTML = '';
  let lastWasCorrect = null;

  for (let char of inputted) {
    if (index < text.length && char === text[index]) {
      if (lastWasCorrect === null) {
        innerHTML += '<span class="correct">';
      } else if (lastWasCorrect === false) {
        innerHTML += '</span><span class="correct">'
      }
      lastWasCorrect = true;
    } else {
      if (lastWasCorrect === null) {
        innerHTML += '<span class="incorrect">';
      } else if (lastWasCorrect === true) {
        innerHTML += '</span><span class="incorrect">';
      }
      lastWasCorrect = false;
    }
    innerHTML += text[index];
    index++;
  }
  if (innerHTML) {
    innerHTML += '</span>';
  }
  if (inputted.length !== text.length) {
    innerHTML += `<span class="next">${text[inputted.length]}</span>`
  }
  innerHTML += text.substring(inputted.length + 1);

  return innerHTML;
}

export const showModal = (gameResult: GameResult[], textToType: string) => {
  const userResultEls = gameResult.map(result => {
    const resultEl: HTMLElement = createElement({ tagName: 'li' });
    resultEl.textContent = `
      ${result.username} | ${result.spentSeconds && (textToType.length / result.spentSeconds).toFixed(2) || '-'} symbols per second
    `;
    return resultEl;
  })

  const backdrop: HTMLElement = createElement({ tagName: 'div', className: 'modal-backdrop' });
  const modal: HTMLElement = createElement({ tagName: 'div', className: 'modal' });
  const modalHeader: HTMLElement = createElement({ tagName: 'div', className: 'modal__header' });
  const gameResultEl: HTMLElement = createElement({ tagName: 'p' });
  const closeIcon: HTMLElement = createElement({ tagName: 'span', className: 'modal__close-icon' });
  const usersList: HTMLElement = createElement({ tagName: 'ol', className: 'modal__user-list' });

  gameResultEl.textContent = 'Game Results';
  modalHeader.append(gameResultEl, closeIcon);
  usersList.append(...userResultEls);
  modal.append(modalHeader, usersList);
  backdrop.append(modal)

  document.body.insertAdjacentElement('beforeend', backdrop);
}

export const calcProgress = (text: string, input: string) => {
  if (!text?.length || !input?.length) {
    return 0;
  }

  let count = 0;
  for (let char of input) {
    if (char === text[count]) {
      count += 1;
    }
  }
  return count * 100 / text.length;
}

export const makeTextInputHandler = (event: KeyboardEvent, user: User, seconds: { secondsPassed: number }) => {
    // if user typed whole text we should no longer let him input data
    if (!user.progress.spentSeconds && (event.key.length === 1 || event.key === 'Backspace')) {

      if (event.key === 'Backspace') {
        user.progress.inputtedText = user.progress.inputtedText.substring(0, user.progress.inputtedText.length - 1);
      }
      else if(user.progress.inputtedText.length < appState.text!.length) {
        user.progress.inputtedText += event.key;
      }
      appState.textElement!.innerHTML = formateInputTextInner(appState.text!, user.progress.inputtedText);
      user.progress.doneInPercents = calcProgress(appState.text!, user.progress.inputtedText);
      if (user.progress.doneInPercents === 100) {
        user.progress.spentSeconds = seconds.secondsPassed;
      }
      appState.socket!.emit(actions.userStateUpdate, user.progress);
    }
};
