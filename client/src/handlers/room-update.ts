import { createElement, addClass, removeClass, } from '../helpers/domHelpers';
import { formateRoomUserView } from '../helpers/domFormatters';
import { User, Room } from '../types/index';
import { actions } from '../constants/actions';
import { appState, AppState, proxyState } from '../state';

export const onRoomsUpdate = () => {
  const socket: SocketIOClient.Socket = appState.socket!;
  const rooms: Room[] = appState.rooms!;
  const roomsContainerPrev = document.querySelector('.rooms-page__rooms-container');
  const roomContainerNext = createElement({ tagName: 'div', className: 'rooms-page__rooms-container rooms-page__item' });
  const roomElements = rooms.map(room => {
    const roomContainer = createElement({ tagName: 'div', className: 'room-container__room' });
    const roomUsersCount = createElement({ tagName: 'p', className: 'room__users-count' });
    const roomName = createElement({ tagName: 'h2', className:'room__name' });
    const roomJoin = createElement({ tagName: 'button', className: 'room__join' });
    roomUsersCount.textContent = `${room.usersCount} user connected`;
    roomName.textContent = room.name;
    roomJoin.textContent = 'Join';
    roomJoin.addEventListener('click', () => socket.emit(actions.joinRoom, room.name));
    roomContainer.append(roomUsersCount, roomName, roomJoin);
    return roomContainer;
  });
  roomContainerNext.append(...roomElements);
  roomsContainerPrev?.replaceWith(roomContainerNext);
  return rooms;
};

export const onJoinRoom = () => {
  const room: Room = appState.activeRoom!;
  const roomsPage: HTMLElement = <HTMLElement>document.querySelector('.rooms-page');
  const gamePage: HTMLElement = <HTMLElement>document.querySelector('.game-page');
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
}

export const onRoomUpdate = (): void => {
  const room: Room = appState.activeRoom!;
  const usersContainerOld: HTMLElement = <HTMLElement>document.querySelector('.game-page__users-container');
  const userElements: string[] = room.users!.map((user: User) => formateRoomUserView(user, appState.username!));
  usersContainerOld!.innerHTML = userElements.join('');
}

export const onUserStateUpdate = (updatedUser: User) => {
  const activeRoom: Room = appState.activeRoom!;
  const user: User = activeRoom.users!.filter((user: User) => user.name === updatedUser.name)[0];
  if (user) {
    (<any>Object).assign(user, updatedUser);
    proxyState.activeRoom = activeRoom;
  }
  return null;
}

export const onBotCommentSet = () => {
  const comment: string = appState.botComment!;

  const commentElement: HTMLElement = <HTMLElement>document.querySelector('.bot-column__bot-text');
  commentElement.textContent = comment;
}