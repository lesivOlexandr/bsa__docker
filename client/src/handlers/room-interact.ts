import { createElement, removeClass, addClass } from '../helpers/domHelpers';
import { formateInputTextInner, showModal, makeTextInputHandler } from '../helpers/domFormatters';
import { Room, GameResult } from '../types/index';
import { actions } from '../constants/actions';
import { partialRight } from 'lodash';

import { appState } from '../state';

export const leaveRoom = (): void => {
  const roomsPage: HTMLElement = <HTMLElement>document.querySelector('.rooms-page');
  const gamePage: HTMLElement = <HTMLElement>document.querySelector('.game-page');
  removeClass(roomsPage, 'display-none');
  addClass(gamePage, 'display-none');
  appState.socket!.emit(actions.leaveRoom);
}

export const handleTimerStart = ({ seconds, textIdx }: { seconds: number, textIdx: number }): Promise<string> => {
  const timerParent: HTMLElement = <HTMLElement>document.querySelector('.game-page__ready-column');
  const readyButton: HTMLElement = <HTMLElement>document.querySelector('.ready-column__ready-button');
  const backToRoomsButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');

  readyButton.hidden = true;
  backToRoomsButton.hidden = true;

  const timerElement: HTMLElement = createElement({ tagName: 'span' });
  timerElement.textContent = String(seconds);
  timerParent.append(timerElement)

  const intervalId = setInterval(() => {
    if (seconds > 1) {
      timerElement.textContent = String(--seconds);
    } else {
      timerParent.removeChild(timerElement);
      clearInterval(intervalId);
    }
  }, 1000);

  return fetch(`/game/texts/${textIdx}`).then(res => res.text()).then(text => appState.text = text);
}

export const handleChallengeStart = (allowedSeconds: number) => {
  const seconds = { allowedSeconds, secondsPassed: 0 };
  const column: HTMLElement = <HTMLElement>document.querySelector('.game-page__ready-column');
  const timerElement: HTMLElement = createElement({ tagName: 'span', className: 'ready-column__game-timer' });
  const textElement: HTMLElement = createElement({ tagName: 'span', className: 'ready-column__text' });
  timerElement.textContent = allowedSeconds + ' seconds left';

  const intervalId = setInterval(() => {
    if (seconds.allowedSeconds > seconds.secondsPassed) {
      timerElement.textContent = (seconds.allowedSeconds - ++seconds.secondsPassed) + ' seconds left';
    } else {
      clearInterval(intervalId);
    }
  }, 1000);
  if (appState.text) {
    textElement.innerHTML = formateInputTextInner(appState.text, '');
    column.append(timerElement, textElement);
    appState.textElement = textElement;
    const user = appState.activeRoom?.users?.find(user => user.name === appState.username);
    appState.handlerFunc = partialRight(makeTextInputHandler, user, seconds) as any;
    document.addEventListener('keydown', appState.handlerFunc!);
  }
}

export const handleChallengeEnd = (gameResult: GameResult[]) => {
  document.removeEventListener('keydown', appState.handlerFunc!);
  showModal(gameResult, appState.text!);
  const closeModalEl: HTMLElement = <HTMLElement>document.querySelector('.modal__close-icon');
  const modalEl: HTMLElement = <HTMLElement>document.querySelector('.modal-backdrop');
  closeModalEl.addEventListener('click', () => modalEl.remove());

  document.querySelectorAll('.ready-column__game-timer, .ready-column__text').forEach(el => el.remove());
  const readyButton: HTMLElement = <HTMLElement>document.querySelector('#ready-button');
  const backToRoomsButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');
  readyButton.hidden = false;
  readyButton.textContent = 'Ready';
  backToRoomsButton.hidden = false;
  appState.text = '';
}
